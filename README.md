# Metallic Kusudama

<img src="img/tdgarden2.png" width=700px center=left>


<img src="img/cbalogo.jpeg" width=500px center=left>


## Introduction

**Kusudama is a form of modular Origam that, by assemblying same-shape cells, generates an enclosed volume shape (sphere like, but the structure is based on classic polyhedron.)**

Some time ago, since I got the last book of Ekaterina Lukasheva [Modern Kusudama Origami](https://kusudama.me/), I had in mind to build edge-unit polyhedra but maybe, one order of magnitude bigger than conventional modular folds... 

Modular Origami heavily relies on octahedron, cubes, icosahedron and dodecahedron. I decided to design my own Kusudama and to do so, I revisited a type of polyhedra that I consider incredibly interesting, the family of [fleible polyhedra](https://en.wikipedia.org/wiki/Flexible_polyhedron). These shapes have DOFs that change the form of the polyhedra without alter the facets. 

Doing so, I could have a geometrical structure in the form of edge-Kusudama but with a degree of freedom. I choosed a [Jeesen's Icosahedron](https://en.wikipedia.org/wiki/Jessen%27s_icosahedron), discovered by the french mathematician [Adrien Douady](https://en.wikipedia.org/wiki/Adrien_Douady). It has an interesting chiral deformation that will be shown later in the documentation. 

<img src="img/jessens.png" width=600px center=left>

As my research is pointing more and more towards folding metals, I decided to make a huge shiny aluminum Kusudama, something I have not seen yet before and I was very excited to see. I have some ideas for next iterations with the material looking. 

A month ago, friends from [Infra](https://www.infrabos.com/) contacted me to make an installation for the Boston Boiler Room and I decided to go with this! Jurgis Ruza and I have worked last week in the nights to make this happen.   




## Structural Design

As I have some spare sheets of Hylite, a 3M aluminum composite with 1.2mm thickness and PP core, I decided this is the best fit material to build the structure out of. I would double face mill the peaks and valleys and join modules with M3 bolts. 

As the Icosahedron is non regular, but still resembles the node counts as a regular one, I just detail designed one node, and then, spatially alocate this design 12 times to get the full model. 

<img src="img/node.png" width=600px center=left>

<img src="img/complete.png" width=600px center=left>


Each node is a folded sheet that holds 5 beams. It uses 36 screws and hosts in the tip a translicent peak that will host a PCB with LEDs to make the nodes to shine! 

<img src="img/bolts.png" width=600px center=left>

All models can be downloaded from the CAD folder. 


I also designed a base to host the power and controll of the structure and to be hanged from the cealing. 

<img src="img/CADbase.png" width=600px center=left>



## Electronic Design

Two PCBs were degidned, one to host the LEDs on the nodes and other to transfer to each of them data and power. The Node board, host very bright LEDs and 360nm wavelength UV leds. Those operates at different volts and are controlled through PWM. To take logic levels to it desired values (5V and 12V), I installed two N-mosfets to each of the LEDs. The schematics and board layouts can be downloaded from CAD. 

<img src="img/nodePCB.png" width=600px center=left>


An external 12V power supply will feed the microcontroller and the LEDs through a second PCB called the distribution board. 

<img src="img/distributionPCB.png" width=600px center=left>

Then, I just had to manufacture 1 of the power and 12 of the distribution boards. I added aluminum tape in the back of the PCBs so they are almost invisible when installed in the nodes of the structure!

<img src="img/pcbmanuf.gif" width=400px center=left>

<img src="img/pcblayout.png" width=600px center=left>


## Manufacturing

<img src="img/flat.jpeg" width=600px center=left>

With everything cutted in the Zund, is time to assembly and put hose 36 * 12 bolts to make this! 

<img src="img/manuf1.gif" width=600px center=left>

<img src="img/manuf2.gif" width=400px center=left>

The structure is done, and circuits are integrated, is blinking and working very well :

<img src="img/blink.gif" width=600px center=left>

## Boiler Room

Now its time to transport the structure to Big Night Live, at the TD Garden. Its was pretty ridiculous to insert that on a pickup. 

<img src="img/pickup.jpeg" width=600px center=left>

<img src="img/tdgarden.png" width=700px center=left>

And the party was a blast. 

<img src="img/boiler.png" width=700px center=left>

<img src="img/boiler2.jpg" width=700px center=left>

(thanks to infra for this pic)
